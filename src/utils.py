import functools
import time


@functools.total_ordering
class Timer:
    def __enter__(self):
        self.start = time.perf_counter()
        return self

    def __exit__(self, *args):
        self.end = time.perf_counter()
        self.duration = self.end - self.start

    def _is_valid_operand(self, other):
        return all(hasattr(other, x) for x in ("start", "end", "duration"))

    def __eq__(self, other):
        if not self._is_valid_operand(other):
            return NotImplemented
        return all(
            getattr(other, x) == getattr(self, x) for x in ("start", "end", "duration")
        )

    def __lt__(self, other):
        if not self._is_valid_operand(other):
            return NotImplemented
        return self.duration < other.duration
