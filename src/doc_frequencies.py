#! /usr/bin/env python

import codecs
import csv
import gzip
from pathlib import Path

# import shutil
# from string import punctuation

# from pke import compute_document_frequency

"""Compute Document Frequency (DF) counts from a collection of documents.

N-grams up to 3-grams are extracted and converted to their n-stems forms.
Those containing a token that occurs in a stoplist are filtered out.
Output file is in compressed (gzip) tab-separated-values format (tsv.gz).
"""

DATA = Path("c:/Users/BSCL0037_PC/Documents/data/stukken")
TXT = Path("c:/Users/BSCL0037_PC/Documents/data/stukken_text")

# TXT.mkdir(exist_ok=True)

# for tf in DATA.glob("**/*.txt"):
#     shutil.copyfile(tf, TXT / tf.name)

# stoplist for filtering n-grams
# stoplist=list(punctuation)

# compute df counts and store as n-stem -> weight values
# compute_document_frequency(input_dir=str(TXT),
#                            output_file=DATA / "freq.tsv.gz",
#                            extension="txt",           # input file extension
#                            language="nl",                # language of files
#                            normalization="stemming",    # use porter stemmer
#                            stoplist=stoplist)

with gzip.open(DATA / "freq.tsv.gz", "rt", encoding="utf-8") as f:
    with open("ngram_freqs.tsv", "w") as tf:
        tf.write(f.read())


def load_document_frequency_file(input_file, delimiter="\t"):
    """Load a tsv (tab-separated-values) file containing document frequencies.
    Automatically detects if input file is compressed (gzip) by looking at its
    extension (.gz).
    Args:
        input_file (str): the input file containing document frequencies in
            csv format.
        delimiter (str): the delimiter used for separating term-document
            frequencies tuples, defaults to '\t'.
    Returns:
        dict: a dictionary of the form {term_1: freq}, freq being an integer.
    """

    # initialize the DF dictionary
    frequencies = {}

    # open the input file
    with gzip.open(input_file, "rt", encoding="utf-8") if input_file.endswith(
        ".gz"
    ) else codecs.open(input_file, "rt", encoding="utf-8") as f:
        # read the csv file
        df_reader = csv.reader(f, delimiter=delimiter)

        # populate the dictionary
        for row in df_reader:
            frequencies[row[0]] = int(row[1])

    # return the populated dictionary
    return frequencies
