#! /usr/bin/env python

import codecs
import csv
from datetime import datetime
import gzip
import json
import logging
from pathlib import Path
from typing import Dict, Any, Optional
import string

# import more_itertools as mi
import pke

from utils import Timer


def underline(string, line_char="=", newline=True):
    ul = f"{string}\n{len(string)*line_char}\n"
    if newline:
        return ul
    return ul.strip()


log_dir = Path(__file__).resolve().parent / "logs"
log_dir.mkdir(exist_ok=True)
log_file = log_dir / f"keywords-{datetime.now().strftime('%d%m%Y%H%M%S')}.txt"
logging.basicConfig(
    filename=log_file,
    filemode="w",
    format="%(levelname)s - %(message)s",
    level=logging.WARNING,
)

log = logging.getLogger(__name__)


def _update_metadata(working_dir: Path, data: Dict[str, Any]) -> bool:
    mdfile = working_dir / "metadata.json"
    if mdfile.exists():
        with mdfile.open() as f:
            metadata = json.load(f)
    else:
        log.warning(f"No metadata file in {working_dir}.")
        metadata = {}

    metadata.update(data)

    with mdfile.open("w") as f:
        json.dump(metadata, f, indent=4)
    return True


def check_metadata(fp: Path, key: str) -> Optional[str]:
    """Get metadata value for key."""
    with (fp / "metadata.json").open() as f:
        md = json.load(f)
    return md.get(key, None)


def textacy_records(warehouse: Path):
    for tf in warehouse.glob("**/*.txt"):
        with tf.open(encoding="utf-8") as f:
            yield (f.read(), {"fname": str(f)})


def load_document_frequency_file(input_file, delimiter="\t"):
    """Load a tsv (tab-separated-values) file containing document frequencies.
    Automatically detects if input file is compressed (gzip) by looking at its
    extension (.gz).
    Args:
        input_file (str): the input file containing document frequencies in
            csv format.
        delimiter (str): the delimiter used for separating term-document
            frequencies tuples, defaults to '\t'.
    Returns:
        dict: a dictionary of the form {term_1: freq}, freq being an integer.
    """

    # initialize the DF dictionary
    frequencies = {}

    # open the input file
    with gzip.open(input_file, "rt", encoding="utf-8") if input_file.endswith(
        ".gz"
    ) else codecs.open(input_file, "rt", encoding="utf-8") as f:
        # read the csv file
        df_reader = csv.reader(f, delimiter=delimiter)

        # populate the dictionary
        for row in df_reader:
            frequencies[row[0]] = int(row[1])

    # return the populated dictionary
    return frequencies


if __name__ == "__main__":

    DATA = Path("c:/Users/BSCL0037_PC/Documents/data/stukken")

    # nl = textacy.load_spacy_lang("nl_core_news_sm")

    for tf in DATA.glob("**/*.txt"):
        print(f"{tf}\n")
        # print(underline("TopicRank"))
        # with Timer() as t:
        #     # initialize keyphrase extraction model, here TopicRank
        #     extractor = pke.unsupervised.TopicRank()

        #     # load the content of the document, here document is expected to be in raw
        #     # format (i.e. a simple text file) and preprocessing is carried out using spacy
        #     extractor.load_document(input=str(tf), language='nl_core_news_sm')

        #     # keyphrase candidate selection, in the case of TopicRank: sequences of nouns
        #     # and adjectives (i.e. `(Noun|Adj)*`)
        #     extractor.candidate_selection()

        #     # candidate weighting, in the case of TopicRank: using a random walk algorithm
        #     extractor.candidate_weighting()

        #     # N-best selection, keyphrases contains the 10 highest scored candidates as
        #     # (keyphrase, score) tuples
        #     print("\n".join(i[0] for i in extractor.get_n_best(n=10)))
        # print(underline(f"\nExtracted in {t.duration:.3f}s"))

        # print(underline("PositionRank"))
        # with Timer() as t:

        #     # 1. create a PositionRank extractor.
        #     extractor = pke.unsupervised.PositionRank()

        #     # 2. load the content of the document.
        #     extractor.load_document(input=str(tf),
        #                             language='nl',
        #                             normalization=None)

        #     # 3. select the noun phrases up to 3 words as keyphrase candidates.
        #     extractor.candidate_selection(maximum_word_number=1)

        #     # 4. weight the candidates using the sum of their word's scores that are
        #     #    computed using random walk biaised with the position of the words
        #     #    in the document. In the graph, nodes are words (nouns and
        #     #    adjectives only) that are connected if they occur in a window of
        #     #    10 words.
        #     extractor.candidate_weighting(window=10)

        #     # 5. get the 10-highest scored candidates as keyphrases
        #     print("\n".join(i[0] for i in extractor.get_n_best(n=10)))
        # print(underline(f"\nExtracted in {t.duration:.3f}s"))

        print(underline("TFIDF"))
        with Timer() as t:

            # 1. create a TfIdf extractor.
            extractor = pke.unsupervised.TfIdf()

            # 2. load the content of the document.
            extractor.load_document(
                input=str(tf), language="nl", normalization="stemming"
            )

            # 3. select {1-3}-grams not containing punctuation marks as candidates.
            extractor.candidate_selection(n=1, stoplist=list(string.punctuation))

            # 4. weight the candidates using a `tf` x `idf`. NB: UnicodeDecodeError

            # with gzip.open(DATA/"freq.tsv.gz", "rt", encoding="utf-8") as f:
            df = load_document_frequency_file(input_file=str(DATA / "freq.tsv.gz"))

            extractor.candidate_weighting(df=df)

            # 5. get the 10-highest scored candidates as keyphrases
            keyphrases = extractor.get_n_best(
                n=10
            )  # 5. get the 10-highest scored candidates as keyphrases
            print("\n".join(i[0] for i in extractor.get_n_best(n=10)))
        print(underline(f"\nExtracted in {t.duration:.3f}s"))
        break
